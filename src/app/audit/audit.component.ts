import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { Audit, User } from '@/_models';
import { AuditService, AuthenticationService } from '@/_services';

@Component({ templateUrl: 'audit.component.html' })
export class AuditComponent implements OnInit
{
    audits = [];

    filterAudits:any[]=[];

    rangeArray:number[]=[10,20,30,50];

    range:number=10;

    page:number=1;

    cloneAudits:any[]=[];

    dateFormat:string='dd/MM/yyyy hh;mm:ss';

    currentUser:User;

    ascNumberSort:boolean=true;

    sortDirection: number=1;

    constructor(
        private authenticationService: AuthenticationService,
        private auditService: AuditService
    )
    {
        this.currentUser = this.authenticationService.currentUserValue;
    }

    ngOnInit()
    {
        this.loadAllAudits();
    }

    private loadAllAudits()
    {
        this.auditService.getAll()
            .pipe(first())
            .subscribe(audits =>{
                this.audits = audits;
                this.cloneAudits = audits
            } );
    }


    applyFilter(value){

        this.audits=value?this.search(value,this.audits):this.cloneAudits
        
    }

    private search (key, array) 
    {
        const final = [];
        for (let i = 0; i < array.length; i++) 
        {
          var searchObj =
           array[i].user +
           array[i].ip+
           array[i].id+
           array[i].loginTime+
           array[i].logoutTime
          if (
            searchObj != null &&
            searchObj.toLowerCase().search(key.toLowerCase()) !== -1
          ) {
            final.push(array[i]);
          }
        }
        return final;
      };


      sortArr(colName:any){
          this.sortDirection=this.sortDirection==1?-1:1;
        this.audits.sort((a,b)=>{
          a= a[colName].toLowerCase();
          b= b[colName].toLowerCase();
          return a.localeCompare(b) * this.sortDirection;
        });
      }

      
}

